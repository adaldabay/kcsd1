#!/bin/bash

# Устанавливаем переменную с путем к директории с файлами
dir="/tmp/test/files/"

# Получаем список файлов в директории и сортируем их по дате изменения, последний файл будет первым в списке
file=$(find "$dir" -type f -mtime -7 | sort -n | head -n 1)

# Проверяем, что переменная file не пуста
if [ -n "$file" ]; then
  # Получаем текущую дату и время
  date=$(date +"%Y-%m-%d %H:%M:%S")
  # Получаем имя файла
  filename=$(basename "$file")
  # Получаем размер файла в байтах
  size=$(du -h "$file" | cut -f1)

  # Записываем информацию о файле в лог
  echo "$date $file $filename $size" >> /tmp/test/script-log/sync.log

  # Создаем архив с именем файла и датой создания архива
  tar -czPf "/tmp/test/sync.log/$filename-$(date +"%Y%m%d%H%M%S").tar.gz" "$file"

  # Перемещаем архив в папку /tmp/test/archives
  mv "/tmp/test/sync.log/$filename-$(date +"%Y%m%d%H%M%S").tar.gz" /tmp/test/archives/

  # Удаляем исходный файл
  rm "$file"
fi

# Добавляем задание в cron для запуска скрипта каждые 15 минут каждый день
(crontab -l ; echo "*/15 * * * * /path/to/script.sh")